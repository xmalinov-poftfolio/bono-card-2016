console.time('json-loading');
var swiffyObjects = [];

var jsons = [
  {
    'name': 'main',
    'url': 'http://2016.bonoagency.ru/data/main.json'
  },
  {
    'name': 'card1',
    'url': 'http://2016.bonoagency.ru/data/card1.json'
  },
  {
    'name': 'card2',
    'url': 'http://2016.bonoagency.ru/data/card2.json'
  },
  {
    'name': 'card3',
    'url': 'http://2016.bonoagency.ru/data/card3.json'
  },
  {
    'name': 'card4',
    'url': 'http://2016.bonoagency.ru/data/card4.json'
  },
  {
    'name': 'card5',
    'url': 'http://2016.bonoagency.ru/data/card5.json'
  }
];

var loadJson = function (filePath, done) {
  var xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function () {
    if (xhr.readyState !== 4) {
      return;
    }
    if (xhr.status !== 200 && xhr.status !== 400) {
      return;
    }
    return done(this.response);
  };
  xhr.open("GET", filePath, true);
  xhr.send();
};

var counter = 0;

jsons.forEach(function (json, i) {
  loadJson(json.url, function (response) {
    var swOb = {};
    swOb.name = json.name;
    swOb.data = JSON.parse(response)[0];

    swiffyObjects.push(swOb);
    console.timeEnd('json-loading');

    if (counter === jsons.length - 1) {
      document.querySelector('#preloader').style.display = 'none';
      loadSwiffy('main');
    }
    counter++;
  });
});

function loadSwiffy(name) {
  if (window.stage) {
    window.stage.destroy();
  }
  window.swiffyobject = swiffyObjects.filter(function (swiffyObject) {
    return swiffyObject.name === name;
  })[0].data;

  window.stage = new swiffy.Stage(document.querySelector('#swiffycontainer'), swiffyobject, {});
  window.stage.start();
}

var currentCard = 'main';

function showCard(num) {
  switch (num) {
    case 4:
      loadSwiffy('card1');
      currentCard = 'card1';
      break;
    case 2:
      loadSwiffy('card2');
      currentCard = 'card2';
      break;
    case 3:
      loadSwiffy('card3');
      currentCard = 'card3';
      break;
    case 1:
      loadSwiffy('card4');
      currentCard = 'card4';
      break;
    case 5:
      loadSwiffy('card5');
      currentCard = 'card5';
      break;
    default:
      console.log('no card');
  }
  endButtons.style.display = 'none';
}

var email = '';
var subject = 'Новогоднее гадание на BONOбананах!';
var message = '"имя", поздравляю с наступающим 2016 годом! Пусть в новом году будет еще больше творческих идей, успехов и счастья! http://2016.bonoagency.ru/';

Event.add(document.querySelector('#sendButton'), 'click', sendEmail);

function sendEmail() {
  var mailtoLink = 'mailto:' + email + '?subject=' + subject + '&body=' + message;
  window.open(mailtoLink, '_blank');
}


var shareURL = '';
var shareTitle = '';
var shareDescription = '';
var shareImage = '';

function getShareMeta() {
  var metas = document.getElementsByTagName('meta');

  for (var i = 0; i < metas.length; i++) {
    if (metas[i].getAttribute('property') == 'og:url') {
      shareURL = metas[i].getAttribute('content');
    }
    if (metas[i].getAttribute('property') == 'og:title') {
      shareTitle = metas[i].getAttribute('content');
    }
    if (metas[i].getAttribute('property') == 'og:description') {
      shareDescription = metas[i].getAttribute('content');
    }
    if (metas[i].getAttribute('property') == 'og:image') {
      shareImage = metas[i].getAttribute('content');
    }
  }
}

function openPopupCenter(url, title, w, h) {
  var wLeft = window.screenLeft ? window.screenLeft : window.screenX;
  var wTop = window.screenTop ? window.screenTop : window.screenY;

  var left = wLeft + (window.innerWidth / 2) - (w / 2);
  var top = wTop + (window.innerHeight / 2) - (h / 2);
  return window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
}

Event.add(document.querySelector('#shareVk'), 'click', shareResult);
Event.add(document.querySelector('#shareFb'), 'click', shareResult);
Event.add(document.querySelector('#shareTw'), 'click', shareResult);
Event.add(document.querySelector('#shareOk'), 'click', shareResult);

function shareResult() {
  var socialType = this.getAttribute('data');
  getShareMeta();
  switch (socialType) {
    case 'fb':
      openPopupCenter('//www.facebook.com/share.php?u=' + shareURL + '&title=' + shareDescription, shareTitle, 655, 327);
      break;
    case 'vk':
      openPopupCenter('//vk.com/share.php?url=' + shareURL + '&title=' + shareTitle + '&description=' + shareDescription + '&image=http://2016.bonoagency.ru/img/bonoagency2016-vk.png', shareTitle, 655, 423);
      break;
    case 'ok':
      openPopupCenter('//www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1&st._surl=' + shareURL + '&st.comments=' + shareDescription, shareTitle, 580, 336);
      break;
    case 'tw':
      openPopupCenter('//twitter.com/share?url' + shareURL + '&text=' + shareDescription, shareTitle, 580, 336);
      break;
    default:
      //
      break;
  }
}
var endButtons = document.querySelector('#end-buttons');
endButtons.style.display = 'none';

function showRepeatButton() {
  endButtons.style.display = 'block';
}

var backButton = document.querySelector('#backButton');
Event.add(backButton, 'click', back);

function back() {
  endButtons.style.display = 'none';
  loadSwiffy('main');
}

var replayButton = document.querySelector('#replayButton');
Event.add(replayButton, 'click', replay);

function replay() {
  endButtons.style.display = 'none';
  loadSwiffy(currentCard);
}


var vis = (function () {
  var stateKey, eventKey, keys = {
    hidden: "visibilitychange",
    webkitHidden: "webkitvisibilitychange",
    mozHidden: "mozvisibilitychange",
    msHidden: "msvisibilitychange"
  };
  for (stateKey in keys) {
    if (stateKey in document) {
      eventKey = keys[stateKey];
      break;
    }
  }
  return function (c) {
    if (c) document.addEventListener(eventKey, c);
    return !document[stateKey];
  }
})();

vis(function () {
  if (!vis()) {
    window.stage.destroy();
    document.getElementById('swiffycontainer').remove();
  } else {
    var sDiv = document.createElement('div');
    sDiv.id = 'swiffycontainer';
    sDiv.className = 'swiffycontainer';
    document.getElementById('swc').appendChild(sDiv);

    //window.stage.destroy();
    loadSwiffy(currentCard);
  }
});

Element.prototype.remove = function () {
  this.parentElement.removeChild(this);
};
NodeList.prototype.remove = HTMLCollection.prototype.remove = function () {
  for (var i = this.length - 1; i >= 0; i--) {
    if (this[i] && this[i].parentElement) {
      this[i].parentElement.removeChild(this[i]);
    }
  }
};